import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

import { PadraoListComponent } from 'app/system/components/padraoList.component';
import { EntidadeService } from '../_shared/entidade.service';
import { Entidade } from '../_shared/entidade.model';
import { ExcelService } from '../_shared/excel.service';

@Component({
  selector: 'app-entidade',
  templateUrl: './entidade.component.html',
  styleUrls: [ './entidade.component.scss' ]
})
export class EntidadeComponent extends PadraoListComponent<Entidade> {
  constructor(
    protected dbService: EntidadeService,
    protected toastrService: ToastrService,
    protected router: Router,
    protected excelService: ExcelService
  ) {
    super(dbService, toastrService, router);
  }

  exportAsXLSX(): void {
    const vLista: any = [];
    this.dbService.getData().subscribe((rRetorno) => {
      rRetorno.forEach((entidade: Entidade) => {
        const vEntidade = new Entidade();
        delete vEntidade.pagamentoAprovado;
        delete vEntidade.usuarioConfirmado;
        vEntidade.nome = entidade.nome;
        vEntidade.whatsapp = entidade.whatsapp;
        vEntidade.email = entidade.email;

        vLista.push(vEntidade);
      });
      //  console.log(vLista);
      this.excelService.exportAsExcelFile(vLista, 'sample');
    });
  }
}
