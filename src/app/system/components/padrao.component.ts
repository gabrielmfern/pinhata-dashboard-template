import {configuration} from '../../config/configuration';
import {UF} from '../../models/shared/ufs.model';
import {UsuarioLocalStorag} from 'app/pages/usuario/_shared/usuario.modal';

export class PadraoComponent {
  public urlRoute: string;
  public VUFs: UF[];
  public keyToDelete: string;
  public Nome_Tabela: string;
  public EstaAlterando = false;

  public UsuarioAtual: UsuarioLocalStorag;

  public NameProjeto = configuration.projectName.firstName + ' ' + configuration.projectName.lastName;

  constructor(protected databaseService?: any) {
    if (databaseService) {
      this.Nome_Tabela = this.databaseService.getNome_Tabela();
      this.urlRoute = '/' + this.databaseService.getUrl();
      this.VUFs = this.databaseService.getUFs();
      this.UsuarioAtual = this.databaseService.getUsuarioLocalStorage();
      // console.log(this.UsuarioAtual);
    }
  }
}
