export const configuration = {
  projectName: {
    firstName: 'Mega',
    lastName: 'Varejo',
    minimalName: 'MV'
  },
  localStorageKey: 'PROJECT-NAME-APP',
  logoPath: '',
  skin: 'skin-purple',
  userDefaultImage: 'assets/default-user.png',
  serverErrorEndpoint: ''
};
// -- Avaiable Skins
// skin-blue
// skin-blue-light
// skin-yellow
// skin-yellow-light
// skin-green
// skin-green-light
// skin-purple
// skin-purple-light
// skin-red
// skin-red-light
// skin-black
// skin-black-light
