export const environment = {
  // Produção

  production: true,
  UrlBackAPI: 'https://us-central1-backmegavarejoon.cloudfunctions.net',
  UrlSite: 'https://www.megavarejoonline.com.br',
  firebaseAppConfig: {
    apiKey: 'AIzaSyA5cRgO8UbIX_ZpcswgVmKsO2BLsYBpp-o',
    authDomain: 'backmegavarejoon.firebaseapp.com',
    databaseURL: 'https://backmegavarejoon.firebaseio.com',
    projectId: 'backmegavarejoon',
    storageBucket: 'backmegavarejoon.appspot.com',
    messagingSenderId: '172992461188',
    appId: '1:172992461188:web:8788a2697f4727f3'
  }

  // ========================================================================
  // testes

  // production: false,
  // UrlBackAPI: 'https://us-central1-backmegavarejoon-dev.cloudfunctions.net',
  // UrlSite: 'https://backmegavarejoon.firebaseapp.com',
  // firebaseAppConfig: {
  //   apiKey: 'AIzaSyAfQqZNzDVrVMikTQ0dzohgwAiDu-pidso',
  //   authDomain: 'backmegavarejoon-dev.firebaseapp.com',
  //   databaseURL: 'https://backmegavarejoon-dev.firebaseio.com',
  //   projectId: 'backmegavarejoon-dev',
  //   storageBucket: 'backmegavarejoon-dev.appspot.com',
  //   messagingSenderId: '255077033384',
  //   appId: '1:255077033384:web:fa071bb1f26479bf'
  // }
};
