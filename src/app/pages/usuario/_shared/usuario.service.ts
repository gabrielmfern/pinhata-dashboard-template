import {Injectable} from '@angular/core';

import {AngularFireDatabase} from 'angularfire2/database';
import {Observable} from 'rxjs';

import {PadraoService} from 'app/system/components/padrao.service';

import {Usuario, UsuarioLocalStorag} from './usuario.modal';
import {UF, Ufs} from 'app/models/shared/ufs.model';

@Injectable()
export class UsuarioService extends PadraoService<Usuario> {
  Dadoselected: Usuario;
  // users: Observable<Usuario[]>;
  sUser: Usuario;

  constructor(protected firebase: AngularFireDatabase) {
    super(firebase, 'usuario', 'Usuario');
    // this.users = this.afb.list<Usuario>(`usuario`).valueChanges();
  }

  createByUid(user: Usuario): Promise<void> {
    return this.firebase.object<Usuario>(`usuario/${user.uid}`).set(user);
  }

  editUser(user: Usuario): Promise<void> {
    return this.firebase.object<Usuario>(`usuario/${user.uid}`).set(user);
  }

  // getUserEmpresa(user: Usuario) {
  //   return this.empresaService.getByKey(user.key_empresa);
  // }

  getByUID(uid: string): Observable<Usuario> {
    const act = this.firebase.object<Usuario>(`usuario/${uid}`).valueChanges();

    return act;
  }

  getByUIDPromise(uid: string): Promise<Usuario> {
    const act = this.getByUID(uid);

    return new Promise<Usuario>(resolve => {
      act.subscribe(user => {
        resolve(user);
      });
    });
  }

  getUFs(): UF[] {
    const VUfs: UF[] = Ufs;
    return VUfs;
  }
}
