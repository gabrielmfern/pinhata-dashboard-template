import { configuration } from '../../config/configuration';

import { Router } from '@angular/router';
import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';

import { AuthService } from '../../providers/services/auth.service';

import { UsuarioLocalStorag } from 'app/pages/usuario/_shared/usuario.modal';



@Component({
  selector: 'app-dash',
  templateUrl: './dash.component.html',
  styleUrls: ['./dash.component.scss']
})
export class DashComponent implements OnInit, OnDestroy {

  public projectName: any = configuration.projectName;
  public userDefault: string = configuration.userDefaultImage;
  private classes: string[] = [configuration.skin, 'sidebar-mini'];
  user: UsuarioLocalStorag;

  constructor(
    public router: Router,
    public auth: AuthService,
    private ngZone: NgZone
  ) {
    this.user = this.auth.getUsuarioLocalStorage();
    // console.log(this.user);
  }

  ngOnInit() {
    this.ngZone.run(() => {
      $(document).ready(() => {
        const layout = $('body').data('lte.layout');
        if (layout) {
          layout.fix();
        }
        const trees: any = $('[data-widget="tree"]');
        if (trees) {
          trees.tree();
        }
      });
      const body = document.getElementsByTagName('body')[0];
      for (const cl of this.classes) {
        body.classList.add(cl);
      }
    })
  }

  isAcLevel(level: 1 | 2 | 3): boolean {
    return this.auth.getUsuarioLocalStorage().nivelacesso >= level;
  }

  logout_old() {
    this.auth.logout().then(() => {
      this.router.navigate(['/login']);
    });
  }

  ngOnDestroy() {
    const body = document.getElementsByTagName('body')[0];
    for (const cl of this.classes) {
      body.classList.remove(cl);
    }
  }

}
