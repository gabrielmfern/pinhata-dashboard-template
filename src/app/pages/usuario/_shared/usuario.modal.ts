import {Endereco} from 'app/models/shared/endereco.model';

export class UsuarioLocalStorag {
  uid: string;
  nome_completo: string;
  email: string;
  nivelacesso: number;
}

export class Usuario {
  uid: string;
  nome_completo: string;
  fantasia: string;
  cpf: string;
  rg: string;
  url_foto?: string;
  fixo: string;
  celular: string;
  email: string;
  senha: string;
  nivelacesso: number;
  endereco: Endereco = new Endereco();
}
