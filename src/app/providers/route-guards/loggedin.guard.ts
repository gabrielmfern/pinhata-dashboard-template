import {Injectable} from '@angular/core';
import {Route, CanActivate, Router} from '@angular/router';

import {AuthService} from '../services/auth.service';

@Injectable()
export class LoggedInGuard implements CanActivate {
  constructor(private auth: AuthService, private router: Router) {}

  canActivate(): boolean {
    const loggedIn = this.auth.isLoggedIn();

    // console.log(loggedIn);
    if (!loggedIn) {
      this.router.navigate(['/login']);
    }
    return loggedIn;
  }
}
