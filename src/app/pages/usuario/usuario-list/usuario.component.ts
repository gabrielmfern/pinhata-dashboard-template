import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {ToastrService} from 'ngx-toastr';

import {PadraoListComponent} from 'app/system/components/padraoList.component';
import {UsuarioService} from '../_shared/usuario.service';
import {Usuario} from '../_shared/usuario.modal';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.scss']
})
export class UsuarioComponent extends PadraoListComponent<Usuario> implements OnInit {
  constructor(protected dbService: UsuarioService, protected toastrService: ToastrService, protected router: Router) {
    super(dbService, toastrService, router);
  }
}
