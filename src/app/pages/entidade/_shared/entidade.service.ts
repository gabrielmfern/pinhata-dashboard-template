import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {AngularFireDatabase} from 'angularfire2/database';

import {PadraoService} from 'app/system/components/padrao.service';
import {Entidade} from './entidade.model';

@Injectable()
export class EntidadeService extends PadraoService<Entidade> {
  Dadoselected: Entidade;

  constructor(protected firebase: AngularFireDatabase) {
    super(firebase, 'entidade', 'Entidade');
  }

  getByKey($key: string): Observable<Entidade> {
    return this.firebase.object<Entidade>(`entidade/${$key}`).valueChanges();
  }
}
