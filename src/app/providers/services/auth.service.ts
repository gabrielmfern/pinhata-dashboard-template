import { Injectable } from '@angular/core';

import * as firebase from 'firebase';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import {
  Usuario,
  UsuarioLocalStorag
} from 'app/pages/usuario/_shared/usuario.modal';

@Injectable()
export class AuthService {
  constructor(private afa: AngularFireAuth, private afb: AngularFireDatabase) {}

  login(user: {
    email: string;
    password: string;
  }): Promise<firebase.auth.UserCredential> {
    const promise = this.afa.auth.signInWithEmailAndPassword(
      user.email,
      user.password
    );

    return new Promise<any>((resolve, reject) => {
      promise
        .then(credential => {
          this.setUsuarioLocalStorage(credential.user.uid).then(() => {
            resolve();
          });
        })
        .catch(reject);
    });
  }

  createAuthUser(user: {
    email: string;
    password: string;
  }): Promise<firebase.auth.UserCredential> {
    return this.afa.auth.createUserWithEmailAndPassword(
      user.email,
      user.password
    );
  }

  sendEmail(pUser: firebase.User) {
    const user = pUser; // firebase.auth().currentUser;

    user
      .sendEmailVerification()
      .then(() => {
        // Email sent.
      })
      .catch(error => {
        // An error happened.
      });
  }

  deleteUser(pUser: firebase.User) {
    const user = pUser; // firebase.auth().currentUser;

    user
      .delete()
      .then(() => {
        // User deleted.
      })
      .catch(error => {
        console.log(error);
        // An error happened.
      });
  }

  retrievePassword(userEmail: string) {
    return this.afa.auth.sendPasswordResetEmail(userEmail);
  }

  logout(): Promise<void> {
    const promise = this.afa.auth.signOut();

    return new Promise<any>((resolve, reject) => {
      promise
        .then(() => {
          this.deleteUsuarioLocalStorage();
          resolve();
        })
        .catch(reject);
    });
  }

  // Silvino Miranda
  getUsuarioLocalStorage(): UsuarioLocalStorag {
    const usuario = JSON.parse(localStorage.getItem('usuario'));
    return usuario;
  }

  // Silvino Miranda
  setUsuarioLocalStorage(pUid: string) {
    return new Promise((resolve, reject) => {
      if (pUid) {
        this.afb
          .object<Usuario>(`usuario/${pUid}`)
          .valueChanges()
          .subscribe(userC => {
            localStorage['usuario'] = JSON.stringify({
              email: userC.email,
              nome_completo: userC.nome_completo,
              nivelacesso: userC.nivelacesso
            });
            console.log('Usuario Criado');
            resolve();
          });
      }
    });
  }

  // Silvino Miranda
  deleteUsuarioLocalStorage() {
    localStorage.removeItem('usuario');
  }

  // Aula de Tarso
  isLoggedIn(): boolean {
    const vUsuarion = this.getUsuarioLocalStorage();
    // console.log(vUsuarion);

    if (vUsuarion) {
      return true;
    } else {
      return false;
    }
  }
}
