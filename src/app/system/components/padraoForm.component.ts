import {OnInit} from '@angular/core';

import {configuration} from 'app/config/configuration';

import {PadraoComponent} from './padrao.component';
import {Usuario} from 'app/pages/usuario/_shared/usuario.modal';

export class PadraoFormComponent extends PadraoComponent implements OnInit {
  public LoggedUser: Usuario;

  // public PName = configuration.projectName.firstName + ' ' + configuration.projectName.lastName;

  public canShow = true;

  constructor(protected databaseService: any) {
    super(databaseService);
  }

  ngOnInit() {}
}
