enum TipoLoja {
  Automoveis = 'Automoveis',
  Varejo_China = 'Varejo China',
  Mercado_Local = 'Mercado Local'
}

export class Entidade {
  $key: string;
  tipoLoja: TipoLoja;
  afiliado: Boolean;
  lojista: Boolean;
  nome: String;
  descricao: String;
  email: String;
  emailOficial: String;
  whatsapp: String;
  facebook: String;
  instagram: String;
  foto: File;

  cep: String;
  cidade: String;
  estado: String;
  bairro: String;

  origem: String;

  pagamentoAprovado: Boolean = false;
  usuarioConfirmado: Boolean = false;
}
