import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';

import {UsuarioService} from 'app/pages/usuario/_shared/usuario.service';

@Injectable()
export class NivelDevGuard implements CanActivate {
  constructor(private usuarioService: UsuarioService, private router: Router) {}

  canActivate(): boolean {
    const NivelAcesso = this.usuarioService.getUsuarioLocalStorage().nivelacesso;

    if (NivelAcesso === 3) {
      return true;
    } else {
      // this.router.navigate(['/']);
      // console.log('voce nao é nivel 3 e nao pode abrir esta pagina');
      return false;
    }
  }
}
