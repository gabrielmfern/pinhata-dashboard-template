import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { EntidadeComponent } from './entidade-list/entidade.component';
import { EntidadeformComponent } from './entidade-form/entidadeform.component';

import { SharedModule } from 'app/system/shared/shared.module';

import { EntidadeService } from './_shared/entidade.service';
import { AdSetService } from 'app/providers/services/AdSet.service';
import { ExcelService } from './_shared/excel.service';

const ROUTES: Routes = [
  { path: '', component: EntidadeComponent, canActivate: [] },
  { path: 'new', component: EntidadeformComponent, canDeactivate: [] },
  { path: 'edit', component: EntidadeformComponent, canDeactivate: [] }
];

@NgModule({
  declarations: [ EntidadeComponent, EntidadeformComponent ],
  imports: [ RouterModule.forChild(ROUTES), ReactiveFormsModule, CommonModule, SharedModule ],
  providers: [ EntidadeService, AdSetService, ExcelService ]
})
export class EntidadeModule {}
